#!/usr/bin/env python3
import argparse
import colorsys
import pyperclip
import random
import sys


def random_color():
    return "#{:06x}".format(random.randint(0, 0xFFFFFF))


def rainbow_colored(text):
    # Count the number of symbols in the string
    num_symbols = len(text)

    # Generate colors in rainbow order
    hue_values = [x/num_symbols for x in range(num_symbols)]
    colors = [colorsys.hsv_to_rgb(hue, 1, 1) for hue in hue_values]

    output = ""
    for i, char in enumerate(text):
        color = colors[i]
        hex_color = "#{:02x}{:02x}{:02x}".format(
            int(color[0]*255), int(color[1]*255), int(color[2]*255))
        output += "[color=" + hex_color + "]" + char + "[/color]"
    return output


def random_colored(text):
    output = ""
    for char in text:
        color = random_color()
        output += "[color=" + color + "]" + char + "[/color]"
    return output


# Set up argparse
parser = argparse.ArgumentParser(description="Generate rainbow-colored or random-colored rich text.")
parser.add_argument("text", metavar="TEXT", help="the text to color")
parser.add_argument("-r", "--random", action="store_true", help="use random colors instead of rainbow colors")
parser.add_argument("-v", "--verbose", action="store_true", help="print output")
args = parser.parse_args()

# Generate rich text based on arguments
if args.text:
    if args.random:
        rich_text = random_colored(args.text)
    else:
        rich_text = rainbow_colored(args.text)
    pyperclip.copy(rich_text)
    print("Rich text copied to clipboard!")
    if args.verbose:
        print("Rich text copied to clipboard:\n" + rich_text)
else:
    parser.print_usage()
